# -*- coding: UTF-8 -*-
'''
Created on Dec 14, 2017

@author: infra
'''
import json, pymssql
from xmljson import badgerfish as bf
from xml.etree.ElementTree import fromstring

from pISCommon.pISLauncher import c
from pyInfraUtils.LogPrinter import p

class BaseHandler():
    '''
    classdocs
    '''    
    
    serverObject = None
    call_proc = ''
    request = None
    result = None
    response = None


    def __init__(self, params):
        '''
        Constructor
        '''
        if('serverObject' not in params):
            return None
        
        self.serverObject = params['serverObject']
        
    def responseJSON(self, response):
        self.serverObject.wfile.write(json.dumps(response).encode("utf-8"))
        
    def sendResponse(self):
        self.serverObject.send_response(200)
        self.serverObject.send_header("Content-type", "text/json")
        self.serverObject.end_headers()
        self.responseJSON(self.response)
        
    def handleRequest(self):    
        if('Usuario' not in self.serverObject.postvars):
            self.responseJSON({"success": 0, "msg": "No se definio el Usuario"})
            return
        else:
            self.serverObject.postvars
        
        if('Contrasena' not in self.serverObject.postvars):
            self.responseJSON({"success": 0, "msg": "No se definio la Contraseña"})
            return 
        
        if('Referencia' not in self.serverObject.postvars):
            self.responseJSON({"success": 0, "msg": "No se definio la Referencia"})
            return 
        
        if('Procesar' not in self.serverObject.postvars):
            self.serverObject.postvars['Procesar'] = 1
            
        if('EliminarProcesado' not in self.serverObject.postvars):
            self.serverObject.postvars['EliminarProcesado'] = 0
        
        #if('params[Acceso]' not in self.serverObject.postvars):    
        #    if('params[IDSocio]' not in self.serverObject.postvars):
        #        self.responseJSON({"success": 0, "msg": "No se definio el ID de Socio"})
        #        return
        #    else:
        #        self.responseJSON({"success": 0, "msg": "No se definio la clave de Acceso"})
        #        return

        postvars = self.serverObject.postvars
        
        result = []
        host = c.cfg("Intelisis", "host")
        user = c.cfg("Intelisis", "user")
        password = c.cfg("Intelisis", "password")
        dbname = c.cfg("Intelisis", "dbname")
        
        with pymssql.connect(host, user, password, dbname, autocommit=True) as conn:
            with conn.cursor(as_dict=True) as cursor:
                # Hacer esto dinamicamente
                #if('params[IDSocio]' in postvars):
                #    Solicitud = '<Solicitud Empresa="'+str(postvars['Empresa'])+'" IDSocio="'+str(postvars['params[IDSocio]'])+'" />'
                #elif('params[Acceso]' in postvars):
                #    Solicitud = '<Solicitud Empresa="'+str(postvars['Empresa'])+'" Acceso="'+str(postvars['params[Acceso]'])+'" />'
                #else:
                Solicitud = '<Solicitud Empresa="'+str(postvars['Empresa'])+'" Usuario="'+str(postvars['Usuario'])+'">'
                for postvar, value in list(postvars.items()):
                    if type(value) == dict:
                        line = '<' + postvar + ' '
                        # ToDo: tambien aqui puede ser duict adentro de otro dict, es necesario hacer esto en una funcion recursiva
                        for var, value in value.items():
                            line = line + var + '="' + value + '" '
                            
                        line = line + ' />'
                    else:
                        line = '<Parametro Nombre="'+str(postvar)+'" Valor="'+str(value)+'" />'
                    Solicitud = Solicitud + line
                
                Solicitud = Solicitud.encode('ascii', 'xmlcharrefreplace').decode("utf-8") + '</Solicitud>'
                
                if('SubReferencia' in postvars):
                    SubReferencia = postvars['SubReferencia'];
                else:
                    SubReferencia = 'ALTA'
                
                Solicitud = '<Intelisis Sistema="Intelisis" Contenido="Solicitud" Referencia="'+postvars['Referencia']+'" SubReferencia="'+SubReferencia+'" Version="1.0">'+Solicitud+'</Intelisis>'
                self.request = Solicitud

                params = (postvars['Usuario'], postvars['Contrasena'], Solicitud, postvars['Procesar'], postvars['EliminarProcesado'], )
                p.print_debug('Proc: %s' % (self.call_proc))
                p.print_debug({'params': params})
                result_proc = cursor.callproc(self.call_proc, params)

                for row in cursor:
                    result = row
                    
        
        p.print_debug({"******Resultado****": result})
        self.result = result
        if 'Resultado' in result:
            response = {
                "success": True,
                # Quitar bf.data y fromstring para no convertir el XML aqui, creo que prefiero hacerlo en PHP pero con el mismo formato
                "ResultadoIS": bf.data(fromstring(result['Resultado'].encode("utf-8"))),
                "ID": result['ID'],
                "Solicitud": result['Solicitud'],
                "msg": result['Mensaje']
                }
        else: 
            response = {
                "success": False,
                "ResultadoIS": "",
                "ID": "",
                "Solicitud": Solicitud,
                "msg": "No se recibio un resultado correcto"
                }

        self.response = response
        p.print_debug({'*********Response******': self.response})
        self.sendResponse()
        return