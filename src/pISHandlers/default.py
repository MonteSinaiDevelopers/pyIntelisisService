'''
Created on Jun 8, 2017

@author: infra
'''

import json
from pISHandlers.BaseHandler import BaseHandler

class defaultHandler(BaseHandler):
    '''
    classdocs
    '''
    
    def handleRequest(self, serverObject):
        serverObject.send_response(200)
        serverObject.send_header("Content-type", "text/json")
        serverObject.end_headers()
        response = {"status": "0"}
        serverObject.wfile.write(json.dumps(response))

if __name__ == '__main__':
    pass