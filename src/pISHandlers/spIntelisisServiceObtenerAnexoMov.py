'''
Created on Dec 14, 2017

@author: infra
'''

from pISHandlers.BaseHandler import BaseHandler
from pyInfraUtils.LogPrinter import p
import os

class spIntelisisServiceObtenerAnexoMovHandler(BaseHandler):
    '''
    classdocs
    '''
    
    call_proc = 'spIntelisisServiceSimple'
    
    def sendResponse(self):
        if(self.response['ResultadoIS'] == ''):
            self.serverObject.send_response(404)
            self.serverObject.end_headers()
            return
        filename = self.response['ResultadoIS']['Intelisis']['Resultado']['AnexoMov']['@Direccion']#.replace('\\', '\\\\')
        p.print_debug(filename)
        f = open(filename, 'rb')

        splitted = os.path.splitext(filename)
        
        self.serverObject.send_response(200)
        self.serverObject.send_header("Content-type", "application/octet-stream")
        self.serverObject.send_header("Content-Disposition", 'attachment; filename="'+self.response['ResultadoIS']['Intelisis']['Resultado']['AnexoMov']['@Nombre']+'"')
        self.serverObject.end_headers()
        self.serverObject.wfile.write(f.read())
        f.close()
        return
        
    
if __name__ == '__main__':
    pass