'''
Created on Jun 7, 2017

@author: infra
'''
import http.server
import importlib, imp
import os
import re
import traceback
from pyInfraUtils.LogPrinter import p

from cgi import parse_header, parse_multipart

from urllib.parse import parse_qs

class pISServerHandler(http.server.SimpleHTTPRequestHandler):
    '''
    classdocs
    '''
    serverObject = None
    postvars = {}
    moduleName = ''
    handler_instance = None
    
    def parse_request_to_module(self):
        path = self.path[1:]
        split = path.split('/')
        
        if(len(split) == 0):
            self.moduleName = 'default'
        else:
            self.moduleName = split[0].strip()
            
        packageName = "pISHandlers."+self.moduleName
            
        try:
            handlers_info = imp.find_module('pISHandlers')
            handlers_package = imp.load_module('pISHandlers', *handlers_info)
            imp.find_module(self.moduleName, handlers_package.__path__)
        except ImportError:
            return self.returnDefaultHandler()
        
        module_imported = importlib.import_module(packageName)
        module_imported = importlib.reload(module_imported)
        
        className = self.moduleName+"Handler"
        constructor = getattr(module_imported, className)
        self.handler_instance = constructor({'serverObject': self})
    
    def parse_POST(self):
        try:
            ctype, pdict = parse_header(self.headers['content-type'])
        except Exception:
            return {}

        if ctype == 'multipart/form-data':
            postvars = parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers['content-length'])
            postvars = parse_qs(
                    self.rfile.read(length), 
                    keep_blank_values=1)
        else:
            postvars = {}

        regexp_pattern = re.compile('^.*\[.*\]+$')
        returned = {}
        for var, value in postvars.items():
            if(type(value) is list):
                var = var.decode('utf-8')
                if(regexp_pattern.match(var) == None):
                    returned[var] = value[0].decode('utf-8')
                else:
                    var_array_value = re.sub('.*\[', '', var).replace(']', '')
                    var_array_key = var.split('[')[0]

                    if var_array_key in returned:
                        if type(returned[var_array_key]) == dict: 
                            returned[var_array_key][var_array_value] = value[0].decode('utf-8')
                        else:
                            returned[var_array_key] = {}
                    else:
                        returned[var_array_key] = {}
                        # ToDo: como aqui tambien puede ser arreglo adentro del arreglo de postvars, es necesario hacer esto en una funcion recursiva
                        returned[var_array_key][var_array_value] = value[0]

        return returned
    
    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_GET(self):
        self.parse_request_to_module()
        if(os.path.isfile(self.translate_path(self.path))):
            return

        p.print_debug('Incoming connection from '+self.client_address[0]+'. Path: '+self.path)
        
        if(self.handler_instance != None):
            try:
                self.handler_instance.handleRequest()
            except BaseException as e:
                self.handler_instance.response = {
                "success": False,
                "ResultadoIS": "",
                "ID": "",
                "Solicitud": self.handler_instance.request,
                "msg": "No se recibio un resultado correcto"
                }
                self.handler_instance.sendResponse()
                p.print_error("Error al manejar una solicitud. Ruta: %s. Error: %s" % (self.path, e))
                p.print_error(traceback.format_exc())
        
    def returnDefaultHandler(self):
        from pISHandlers.default import defaultHandler
        instance = defaultHandler({'serverObject': self}) 
        instance.handleRequest(self)
        return
        
    def do_POST(self):
        self.postvars = self.parse_POST()
        return self.do_GET()
    
    def handle_error(self, request, client_address):
        p.print_error("Error no manejado")
        return

    def log_message(self, format, *args):
        p.print_debug("%s - - [%s] %s\n" % (self.client_address[0], self.log_date_time_string(), format%args))
        #SimpleHTTPServer.SimpleHTTPRequestHandler.log_message(self, format, *args)