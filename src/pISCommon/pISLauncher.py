'''
Created on Jun 5, 2017

@author: infra
'''

import http.server, signal
from pyInfraUtils.LogPrinter import p
from pyInfraUtils.Config import pIUConfig
from pISCommon.pISServer import pISServerHandler

def signal_handler(signal, frame):
    p.print_info("CTRL+C detectado")
    import sys 
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

c = pIUConfig()

port = c.cfg("General", "port")
PORT_NUMBER = int(port)

server_class = http.server.HTTPServer
serverObject = server_class(("", PORT_NUMBER), pISServerHandler)

def launchServer():  
    p.print_success("Iniciando servidor en puerto %s " % PORT_NUMBER)
    p.print_debug("Modo Debug")
    
    try:
        serverObject.serve_forever()
    except KeyboardInterrupt:
        pass
    except BaseException as e:
        p.print_error(str(e))
		
	#serverObject.server_close()
		
def stopServer():
	p.print_success("Deteniendo servidor")
	serverObject.shutdown()
	serverObject.server_close()

if __name__ == '__main__':
    pass