#!/usr/bin/python3
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "src"))
os.chdir(os.path.dirname(os.path.realpath(__file__)))

from pISCommon import pISLauncher
import win32serviceutil
import win32service
import win32event
import servicemanager
import socket

class AppServerSvc (win32serviceutil.ServiceFramework):
	_svc_name_ = "pIS"
	_svc_display_name_ = "pyIntelisisService"

	def __init__(self,args):
		win32serviceutil.ServiceFramework.__init__(self,args)
		self.hWaitStop = win32event.CreateEvent(None,0,0,None)
		#self.stop_event = win32event.CreateEvent(None,0,0,None)
		socket.setdefaulttimeout(60)
		self.stop_requested = False

	def SvcStop(self):
		servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
							  servicemanager.PYS_SERVICE_STOPPED,
							  (self._svc_name_,''))
		pISLauncher.stopServer()
		self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
		win32event.SetEvent(self.hWaitStop)
		#win32event.SetEvent(self.stop_event)
		self.stop_requested = True

	def SvcDoRun(self):
		servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
							  servicemanager.PYS_SERVICE_STARTED,
							  (self._svc_name_,''))
		try:
			pISLauncher.launchServer()
	    except BaseException as e:
	        pass

if __name__ == '__main__':
	win32serviceutil.HandleCommandLine(AppServerSvc)