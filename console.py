#!/usr/bin/python3
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "src"))
os.chdir(os.path.dirname(os.path.realpath(__file__)))

from pISCommon import pISLauncher

pISLauncher.launchServer()

if __name__ == '__main__':
	pass